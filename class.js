class Project{
    constructor(id, course, lecturer, duration, room){
        this.id = id;
        this.course = course;
        this.lecturer = lecturer;
        this.duration = duration;
        this.room = room;
    } 
}
project1 = new Project(1, "JS", "Anna", 48, 1 );
project2 = new Project(2, "HTML, CSS", "Armen", 12, 2 );
project3 = new Project(3, "TS", "Sargis", 14, 3 );
project4 = new Project(4, "ANGULAR2", "Anna", 18, 4 );
project5 = new Project(5, "ES6", "Anna", 18, 5 );
var courseList = [];
courseList.push(project1, project2, project3, project4, project5);
console.log(courseList);
var objLength1 = courseList.map((value,index,courseList)=>{
    let length = Object.keys(value).length;
    console.log(length);
    return length
});
console.log(objLength1);
var max1=0;
for (let i of objLength1) {
    if(i > max1) {
        max1 = i;
    }
}
console.log(max1);
var table1 = document.getElementById("table1");
for(let i = 0; i < courseList.length; i++){
    let row = table1.insertRow();
    for(let j = 0; j < max1; j++){     
        let cell = row.insertCell();
        cell.innerHTML = 000;
    }
}
function saveCourse(){
    var items = document.getElementById("myForm1").elements;
    var row=table1.insertRow();
    for(var i=0; i<items.length; i++){
        var cell = row.insertCell();
        var item = items[i].value;
        cell.innerHTML=item;
    }
}

class Student {
    constructor(id, name, surname, age, course){
        this.id = id;
        this.name=name;
        this.surname=surname;
        this.age=age;
        this.course = course;
    }  
}
student1=new Student(1, "Lusine", "Iskandaryan", 32, "JS" );
student2=new Student(2, "Alla", "Ananyan", 30, "HTML, CSS" );
student3=new Student(3, "Artur", "Petrosyan", 28, "JS" );
student4=new Student(4, "Lusine", "Iskandaryan", 32, "ANGULAR2" );
student5=new Student(5, "David", "Aslanyan", 25, "ANGULAR2" );
student6=new Student(6, "Artur", "Petrosyan", 28, "TS" );
student7=new Student(7, "Lusine", "Iskandaryan", 32, "TS" );
student8=new Student(8, "Alla", "Ananyan", 30, "JS");
student9=new Student(9, "Lusine", "Iskandaryan", 32, "JS" );
var studentList=[];
studentList.push(student1, student2, student3, student4, student5, student6, student7, student8, student9);
console.log(studentList);
//var students=[];
var objLength2 = studentList.map((value,index,studentList)=>{
    let length = Object.keys(value).length;
    console.log(length);
    return length
});
console.log(objLength2);
var max2 = 0;
for (let i of objLength2) {
    if(i > max2) {
        max2 = i;
    }
}
console.log(max2);
var table2 = document.getElementById("table2");
for(let i = 0; i < studentList.length; i++){
    let row = table2.insertRow();
    for(let j = 0; j < max2; j++){     
        let cell = row.insertCell();
        cell.innerHTML = 111;
    }
}
function saveStudent(){
    var student=[];
    var items = document.getElementById("myForm2").elements;
    var row=table2.insertRow();
    for(var i=0; i<items.length; i++){
        var cell = row.insertCell();
        var item = items[i].value;
        student.push(item); 
        cell.innerHTML=item;    
    }
    console.log(student);
    studentList.push(student);
    return student;
}



